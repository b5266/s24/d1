//Query Operators

//[SECTION] Comparison Query Operators

//Greater Than/Greater Than or equal
/*
	Syntax:
		$gt/$gte: number
*/
//find users with an age greater than 50
db.users.find({
	age: {
		$gt: 50
	}
})
// //find users with an age greater than or equal to50
db.users.find({
	age: {
		$gte: 50
	}
})
//Less Than/Less Than or equal
/*
	Syntax:
		$lt/$lte: number
*/
////find users with an age less than 50
db.users.find({
	age: {
		$lt: 50
	}
})
//find users with an age less than or equal to 50
db.users.find({
	age: {
		$lte: 50
	}
})
//find users with an age that is Not Equal to 82
db.users.find({
	age: {
		$ne: 82
	}
})

//find users whose last names are either "Hawking" or "Doe"
db.users.find({
	lastName: {
		$in: ["Hawking", "Doe"]
	}
})

/*db.users.find({
	lastName: "Hawking"
})*/

//find users whose courses including "HTML" or "React"
db.users.find({
	courses: {
		$in: ["HTML", "React"]
	}
})

// [SECTION] Logical Query Operators

//$or operator
db.users.find({
	$or: [
		{
				firstName: "Neil"
	    },
	    {
	    	age: 21
	    }
	]
})

db.users.find({
	$or: [
		{
			lastName: "Hawking"
	    },
	    {
	    	lastName: "Doe"
	    }
	]
})

/*Syntax:
		$and operator
*/
db.users.find({
	$and: [
			{
				age: {
						$ne: 82
	   		},
	    		age: {
						$ne: 76
	    	}
	  	}
	]
})

//Inclusion
/*
-allows us to include/add specific fields

*/

db.users.find(
{
	firstName: "Jane"
},
{
	firstName: 0,
	lastName: 0,
	contact: 0,
})

// Exclusion
/*
- allows us to exclude
-The value provided is 0 to denote that the 
-Syntax

*/
db.users.find(
{
	firstName: "Jane"
},
{
	contact: 0,
	department: 0
});

// Suppressing the ID Field
/*	
- Allows us to exclude the "_id" field when retrieving documents.
-When Using field projection, field inclusion and exclusion may not be used at the same time.

-Excluding the "_id" field is the only exception to this rule.
*/ 

db.users.find(
{
	firstName: " Jane",
},
{
	firstName: 1,
	lastName: 1,
	contact: 1,
	_id: 0
})

// Returning Specific Fields in Embedded Documents.
db.users.find(
{
	firstName: 
})

// $regex operator
/*
-Allow us to find documents that match a specific
-string pattern using regular expressions.

-Syntax
	db.users.find({field: {$regex: 'pattern, $options: '$optionValue'}})
*/
// Case sensitive query
db.users.find({firstName: {$regex: 'N'} }).pretty();

// Case insensitive query
db.users.find({firstName: {$regex: 'j', $options: '$i'} });

db.users.find({
	$or: [
		{ firstName: { $regex: 's', $options: '$i' } },
		{ lastName: { $regex: 'd', $options: '$i' }}
	]
}, {firstName: 1, lastName: 1, _id: 0 }).pretty();